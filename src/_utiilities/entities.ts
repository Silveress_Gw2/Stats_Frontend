export interface Account {
    user: string;
    accountLevel: number;
    donation: {
        private: number;
        patreon: number;
        custom: number;
    };
    firstAdded?: string;
    password: string;
    email:string;
    lists: {
        [propName: string]: string;
    };
    emailDetails: {
        account: string;
        password: string;
    };
    verification:{
        [propName: string]: string;
    };
    gameAccounts: {
        [propName: string]: Account_Game;
    };
}
export interface Account_Game {
    key: string;
    permissions: string[];
}