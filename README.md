# DataWars2

## Info
This is the source for my GuildWars2 website named DataWars2.

For how to run/edit/contribute please check out the [Contributing Document](https://gitlab.com/Silvers_Gw2/Stats_Frontend/blob/master/CONTRIBUTING.md)

Feel free to contact me on:


* Gitlab [by creating an issue](https://gitlab.com/Silvers_Gw2/Stats_Frontend/issues/new)
* Reddit: Silveress_Golden
* Discord: @Silver#5563 
* Discord Server: https://discord.gg/m3G33ur
